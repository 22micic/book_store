from django.urls import path
from .views import UserStatisticsGenderView, UserStatisticsDesignationView, TestView, ListBooksByAuthorsView, \
    ListAuthorsByBookView, SoldByStudentView, BooksSoldByAndQuantityView

urlpatterns = [
    path('statistics/gender-report/', UserStatisticsGenderView.as_view()),
    path('statistics/designation-report/', UserStatisticsDesignationView.as_view()),

    path('test/', TestView.as_view()),

    path('statistics/author-book-lists/', ListBooksByAuthorsView.as_view()),
    path('statistics/book-author-list/', ListAuthorsByBookView.as_view()),

    path('statistics/sold-student/', SoldByStudentView.as_view()),

    path('statistics/sold-by', BooksSoldByAndQuantityView.as_view()),

]
