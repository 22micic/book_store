from django.db.models import Count
from rest_framework.generics import ListAPIView
from rest_framework.permissions import IsAuthenticated, AllowAny
from rest_framework import filters
from Users.models import User, Profile
from Store.models import Book, BookAuthor, Author
from .serializers import (UserStatisticsSerializer, ProfileStatisticsFilterSerializer, BarSerializer,
                          BookAuthorsSerializer, SoldByStudentSerializer, SoldBySerializer
                          )


class UserStatisticsGenderView(ListAPIView):
    """
    A class that uses a '.get_queryset(self):' method
    to count the number of user with the same gender.
    """
    permission_classes = [IsAuthenticated]
    serializer_class = UserStatisticsSerializer
    queryset = Profile.objects.all()
    search_fields = ('gender', )

    def get_queryset(self):
        queryset = Profile.objects.filter(gender__in=self.request.query_params.get('gender', None)).values('gender').annotate(Count('gender'))
        print(str(queryset.query))
        return queryset


class UserStatisticsDesignationView(ListAPIView):
    """
    Filter by designation.
    Can also add other fields to filter by.
    """
    permission_classes = [AllowAny]
    serializer_class = ProfileStatisticsFilterSerializer
    queryset = User.objects.all()
    filter_backends = (filters.SearchFilter,)
    search_fields = ['profile__designation', ]


class TestView(ListAPIView):
    """
    TestView is used to test snippet code
    """

    permission_classes = [AllowAny]
    serializer_class = BarSerializer
    queryset = Profile.objects.all()


class ListBooksByAuthorsView(ListAPIView):
    """Search for a book by Author"""
    permission_classes = [AllowAny]
    serializer_class = BookAuthorsSerializer
    queryset = BookAuthor.objects.all()
    filter_backends = (filters.SearchFilter,)
    search_fields = ['author__first_name', 'author__last_name']


class ListAuthorsByBookView(ListAPIView):
    """Search for an Author by book"""
    permission_classes = [AllowAny]
    serializer_class = BookAuthorsSerializer
    queryset = BookAuthor.objects.all()
    filter_backends = (filters.SearchFilter, )
    search_fields = ['book__title', ]


class SoldByStudentView(ListAPIView):
    """Search for book by students name"""
    permission_classes = [AllowAny]
    serializer_class = SoldByStudentSerializer
    queryset = Book.objects.all()
    filter_backends = (filters.SearchFilter,)
    search_fields = ['student__first_name', 'student__last_name']


class BooksSoldByAndQuantityView(ListAPIView):
    """
    Lists all Books, their sellers Full Name if it's a student, and type of seller.
    If it's a Store it only lists Store's name. And it also list current stock of that book.
    """
    permission_classes = [AllowAny]
    serializer_class = SoldBySerializer
    queryset = Book.objects.all()





