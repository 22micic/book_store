from django.contrib import admin


# Register your models here.
from .models import Profile, Gender


admin.site.register(Profile)
admin.site.register(Gender)
