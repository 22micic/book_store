from django.db import models
from django.contrib.auth.models import User


class Gender(models.Model):
    id = models.CharField(primary_key=True, max_length=1, default='', )
    gender_description = models.CharField(max_length=10, default='')

    def __str__(self):
        return self.id


class Profile(models.Model):
    user = models.OneToOneField(User, on_delete=models.CASCADE)
    photo = models.ImageField(blank=True, null=True)
    designation = models.CharField(max_length=20, null=False, blank=False)
    salary = models.IntegerField(null=True, blank=True)
    bio = models.TextField(max_length=500, blank=True)
    city = models.CharField(max_length=30, blank=True)
    birth_date = models.DateField(null=True, blank=True)
    gender = models.ForeignKey(Gender, on_delete=models.CASCADE, null=True)

    def __str__(self):
        return "{0} - {1}".format(self.user.username, self.designation)
