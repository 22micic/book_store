from django.contrib.auth.models import User, Group

from rest_framework.generics import ListCreateAPIView, RetrieveUpdateDestroyAPIView, CreateAPIView, ListAPIView
from rest_framework.permissions import IsAuthenticated, AllowAny

from .serializers import MsgUserSerializer, GroupSerializer, MsgUserRegistrationSerializer, MsgUserDetailSerializer
from oauth2_provider.contrib.rest_framework import TokenHasReadWriteScope


class MsgUserListView(ListCreateAPIView):
    """
    Prints and Creates users
    """
    permission_classes = [IsAuthenticated, TokenHasReadWriteScope]
    serializer_class = MsgUserSerializer
    queryset = User.objects.all().order_by('id')


class MsgUserDetailView(RetrieveUpdateDestroyAPIView):
    """
    Prints a single user and allows the user to update(put method or patch method) or to delete a user
    http_method_names - Restricts methods which developer wants for the user to use.
    """
    permission_classes = [IsAuthenticated, TokenHasReadWriteScope]
    serializer_class = MsgUserDetailSerializer
    queryset = User.objects.all().order_by('id')
    # http_method_names = ['get', 'patch', 'delete', 'put', 'head', 'options']


class MsgUserRegistrationView(CreateAPIView):
    """
    User registration view which allows the the user to sign up without a access token.
    permissions.AllowAny gives use that, but generics.CreateAPIView allows only post method
    so that no unauthorized users cant access the data.
    """
    permission_classes = [AllowAny]
    serializer_class = MsgUserRegistrationSerializer
    queryset = User.objects.all().order_by('id')


class GroupList(ListAPIView):
    """
    Prints and creates groups
    """
    permission_classes = [AllowAny]
    serializer_class = GroupSerializer
    queryset = Group.objects.all().order_by('name')
