# parameter = Parameter.objects.raw("select id, parameter from store_parameter where id = 1")

# for p in parameter:
#     if parameter:
#         serializer_class = SaleDetailSerializer
#         queryset = Sale.objects.all()
#     elif not parameter:
#         serializer_class = SaleDetailTrySerializer
#         queryset = Sale.objects.all()


# class ParameterView(APIView):
#     permission_classes = [AllowAny]
#     parameter = Parameter.objects.filter(parameter=True)
#
#     if parameter:
#         serializer_class = SaleDetailTrySerializer
#         queryset = Sale.objects.all()
#     elif not parameter:
#         serializer_class = SaleDetailSerializer
#         queryset = Sale.objects.all()

# qs = ('''insert into
#             store_saledetail(quantity, price, book_id, sale_id)
#         select
#             ''' + quantity + ''' , (''' + quantity + ''' * sb.price), ''' + book_id + ''',  ''' + sale_id + '''
#         from
#             store_saledetail sd
#                     inner join
#             store_book sb on ''' + book_id + ''' = sb.id
#         limit 1''')


# c = connection.cursor()
# c.execute('insert into store_saledetail(quantity, price, book_id, sale_id) select %s, (%s * sb.price), %s, %s from store_saledetail sd inner join store_book sb on %s = sb.id limit 1', [quantity, quantity, book_id, sale_id, book_id])


# def post(self, request, *args, **kwargs):
#     quantity = self.request.data.get('quantity')
#     book_id = self.request.data.get('book')
#     sale_id = self.request.data.get('sale')
#
#     c = connection.cursor()
#     qs = ('''insert into store_saledetail(quantity, price, book_id, sale_id) values (''' + quantity + ''' , (''' + quantity + ''' * (select price from store_book where id = ''' + book_id + ''')), ''' + book_id + ''', ''' + sale_id + ''')''')
#     qs_update = '''update store_book set store_book.stock = (store_book.stock - ''' + quantity + ''') where store_book.id = (''' + book_id + ''')'''
#     qs2 = '''select id, price, quantity, book_id, sale_id from store_saledetail sd where sd.book_id = ''' + book_id + ''' and sd.quantity = ''' + quantity + ''' and sd.price = (''' + quantity + ''' * (select sb.price from store_book sb where id = ''' + book_id + ''')) limit 1'''
#
#     c.execute(qs)
#     c.execute(qs_update)
#     c.execute(qs2)
#     result = c.fetchall()
#     print(qs)
#     print(qs_update)
#     print(qs2)
#
#     return Response(result, status=201)

# queryset = '''select sb.title as "title", concat(st.first_name, ' ', st.last_name) as "full_name", sty.type as "seller", sb.stock as "quantity"
#                      from store_book sb left join store_student st on sb.student_id = st.id left join store_type sty on sb.seller_type_id = sty.id'''


# if number is None:
#     raise serializers.ValidationError({'number': 'number_not_set '})


# def create(self, validated_data):
#     seller_type = validated_data.pop('seller_type')
#     category_data = validated_data.pop('category')
#     condition_data = validated_data.pop('condition')
#     student_data = validated_data.pop('student')
#
#     type_obj = Type.object.get(id=seller_type['id'])
#
#     return self.Book.objects.create(seller_type=seller_type, **validated_data)

# def to_representation(self, instance):
#     representation = super().to_representation(instance)
#     college_representation = representation.pop('college')
#
#     for key in college_representation:
#         representation[key] = college_representation[key]
#
#     return representation


# @api_view(['GET', 'POST'])
# def sale_by_credit_with_helper(request):
#     if request.method == 'POST':
#
#         c = connection.cursor()
#
#         return_data = {}
#
#         customer_id = request.data.get('customer_id')
#         sale_detail = request.data['sale_detail']
#
#         qs1 = '''START TRANSACTION;
#                              SELECT
#                                 @id := MAX(id) + 1
#                              FROM
#                                 store_sale;
#
#                     insert into store_sale(id, date, customer_id) values(@id, now(), ''' + customer_id + ''');'''
#         c.execute(qs1)
#
#         for request in sale_detail:
#             quantity = request['quantity']
#             book_id = request['book']
#
#             qs = '''insert into store_saledetail(quantity, price, book_id, sale_id) values (''' + quantity + ''' , (''' + quantity + ''' * (select price from store_book where id = ''' + book_id + ''')), ''' + book_id + ''', @id);'''
#
#             c.execute(qs)
#
#             update_book_stock(quantity, book_id)
#
#             qs_select = ''' SELECT
#                                 sd.price as total
#                             FROM
#                                 store_sale ss
#                                     INNER JOIN
#                                 store_saledetail sd  on ss.id = sd.sale_id
#                             WHERE
#                                 ss.id = @id;'''
#             c.execute(qs_select)
#
#             qs_total = c.fetchone()
#             total = qs_total[0]
#
#             qs_credits = check_customer_credit(customer_id)
#
#             total_num = float(total)
#
#             if total_num > qs_credits:
#                 c.execute(""" rollback; """)
#                 raise serializers.ValidationError({'credit': 'insufficient funds'})
#
#             credit_var = str(qs_credits)
#
#             update_customer_credit(credit_var, total, customer_id)
#
#         c.execute(''' commit; ''')
#
#         qs_select = ''' SELECT
#                                 ss.id,
#                                 ss.customer_id,
#                                 ss.date,
#                                 sd.book_id,
#                                 sd.quantity,
#                                 format(sd.price, 2) as total
#                             FROM
#                                 store_sale ss
#                                     INNER JOIN
#                                 store_saledetail sd  on ss.id = sd.sale_id
#                             WHERE
#                                 ss.id = @id;'''
#
#         c.execute(qs_select)
#         results = c.fetchall()
#
#         temp = []
#
#         for result in results:
#             if results.index(result) == 0:
#                 obj = {
#                     'id': result[0],
#                     'customer': result[1],
#                     'date': result[2],
#                     'sale_detail': []
#                 }
#
#             obj1 = {
#                 "book": result[3],
#                 "quantity": result[4],
#                 "total": result[5]
#             }
#             temp.append(obj1)
#
#             obj['sale_detail'] = temp
#
#             return_data = obj
#
#         c.close()
#
#         return Response(return_data, status=201)

# parameter = Parameter.objects.filter(parameter=True)
# parameter = Parameter.objects.values('parameter').filter(id='1')

# print(qs[0])
# param = parameter[0]

# for key, value in param.items():
#     params = value

# def save(self, *args, **kwargs):
#     if self.__class__.objects.count():
#         self.pk = self.__class__.objects.first().pk
#         super().save(*args, **kwargs)
