from django.db import connection


def check_customer_credit(customer_id):

    c = connection.cursor()

    qs_credits = ("""   SELECT
                            sc.credit
                        FROM
                            store_customer sc
                        WHERE
                            id = '""" + customer_id + """' """)
    c.execute(qs_credits)
    credit_amount = c.fetchone()

    return credit_amount[0]


def update_customer_credit(total_price, customer_id):
    c = connection.cursor()

    total = str(total_price)

    qsc_update = """UPDATE
                        store_customer
                    SET
                        credit = credit - """ + total + """
                    WHERE
                        id = """ + customer_id + """; """

    c.execute(qsc_update)


def update_book_stock(quantity, book_id):
    c = connection.cursor()

    qs_update = '''update store_book set store_book.stock = (store_book.stock - ''' + quantity + ''') where store_book.id = (''' + book_id + ''')'''

    c.execute(qs_update)


def check_total_price(sale_detail):
    c = connection.cursor()

    total_price = []

    sum = 0

    for request in sale_detail:
        quantity = request['quantity']
        book_id = request['book']

        qs_total_check = """ SELECT 
                                SUM(price * """ + quantity + """)
                            FROM
                                store_book
                            WHERE
                                id = """ + book_id + """ """

        c.execute(qs_total_check)
        result = c.fetchone()
        total_price.append(result[0])

    for num in total_price:
        sum += num

    return sum


def check_books_stock_sum():

    c = connection.cursor()

    qs_sum = """SELECT 
                   FORMAT(SUM(price * stock), 2) AS total
                FROM
                   store_book"""
    c.execute(qs_sum)

    results_sum = c.fetchone()

    total = results_sum[0]

    return total