from django.core.validators import RegexValidator, MinLengthValidator, MaxLengthValidator, MaxValueValidator, MinValueValidator
from django.db import models


class Parameter(models.Model):
    parameter = models.BooleanField(null=False, blank=False)

    def __str__(self):
        return "{0}".format(self.parameter)


class Author(models.Model):
    first_name = models.CharField(max_length=30, blank=False, null=False)
    last_name = models.CharField(max_length=30, blank=False, null=False)

    def __str__(self):
        return "{0} {1}".format(self.first_name, self.last_name)


class Category(models.Model):
    category_description = models.CharField(max_length=10, default='')

    def __str__(self):
        return self.category_description


class Condition(models.Model):
    condition = models.CharField(max_length=10, default='')

    def __str__(self):
        return self.condition


class College(models.Model):
    college = models.CharField(max_length=30, default='')

    def __str__(self):
        return "{0}. {1}".format(self.id, self.college)


class Student(models.Model):
    first_name = models.CharField(max_length=30, blank=False, null=False)
    last_name = models.CharField(max_length=30, blank=False, null=False)
    index_number = models.CharField(max_length=10, null=True, blank=True)
    college = models.ForeignKey(College, on_delete=models.CASCADE, default='')
    email = models.EmailField(blank=True)
    phone = models.CharField(max_length=17, blank=True, null=True)

    def __str__(self):
        return "{0} {1}".format(self.first_name, self.last_name)


class Type(models.Model):
    type = models.CharField(max_length=10, null=True, blank=True)

    def __str__(self):
        return self.type


class Book(models.Model):
    title = models.CharField(max_length=50, blank=False, null=False)
    summary = models.CharField(max_length=200, null=True, blank=True)
    category = models.ForeignKey(Category, on_delete=models.CASCADE, null=False)
    condition = models.ForeignKey(Condition, on_delete=models.CASCADE, null=False)
    seller_type = models.ForeignKey(Type, on_delete=models.CASCADE, null=True, blank=True)
    student = models.ForeignKey(Student, on_delete=models.CASCADE, null=True, blank=True)
    price = models.DecimalField(blank=False, null=False, max_digits=6, decimal_places=2)
    stock = models.IntegerField(blank=False, null=False)

    def __str__(self):
        return self.title


class BookAuthor(models.Model):
    author = models.ForeignKey(Author, on_delete=models.CASCADE)
    book = models.ForeignKey(Book, on_delete=models.CASCADE)

    class Meta:
        ordering = ['book']

    def __str__(self):
        return "{0} - {1}".format(self.book, self.author)


class CreditCardType(models.Model):
    card_type = models.CharField(max_length=20, blank=False, null=False)

    def __str__(self):
        return self.card_type


class CreditCard(models.Model):
    card_holder_name = models.CharField(max_length=200, null=False, blank=False)
    card_type = models.ForeignKey(CreditCardType, on_delete=models.DO_NOTHING, null=False, blank=False)
    card_number = models.CharField(max_length=50, null=False, blank=False, validators=[RegexValidator(r'^[0-9]*$', 'Only 0-9 are allowed.', 'Invalid Number'), MinLengthValidator(4), MaxLengthValidator(20), ],)
    expiration_month = models.CharField(max_length=10, null=False, blank=False, validators=[RegexValidator(r'^[0-9]*$', 'Only 0-9 are allowed.', 'Invalid Number'), MinLengthValidator(2), MaxLengthValidator(2), ])
    expiration_year = models.CharField(max_length=10, null=False, blank=False, validators=[RegexValidator(r'^[0-9]*$', 'Only 0-9 are allowed.', 'Invalid Number'), MinLengthValidator(4), MaxLengthValidator(4), ],)
    security_code = models.CharField(max_length=30, null=False, blank=False, validators=[RegexValidator(r'^[0-9]*$', 'Only 0-9 are allowed.', 'Invalid Number'), MinLengthValidator(3), MaxLengthValidator(4), ],)

    def __str__(self):
        return "{0} - {1}".format(self.card_holder_name, self.card_type)


class Customer(models.Model):
    first_name = models.CharField(max_length=30, blank=False, null=False, default='')
    last_name = models.CharField(max_length=30, blank=True, null=True, default='')
    email = models.EmailField(blank=True, null=True)
    phone = models.CharField(max_length=17, blank=True, null=True)
    credit_card = models.ForeignKey(CreditCard, on_delete=models.DO_NOTHING, null=True, blank=True)
    credit = models.DecimalField(default=0, null=True, blank=True, max_digits=6, decimal_places=2)

    def __str__(self):
        return "{0} {1}".format(self.first_name, self.last_name)


class Sale(models.Model):
    date = models.DateTimeField(auto_now=True)
    customer = models.ForeignKey(Customer, on_delete=models.CASCADE, null=True, blank=True, default='')

    def __str__(self):
        return "{0} - {1} {2}".format(self.id, self.customer.first_name, self.customer.last_name)


class SaleDetail(models.Model):
    sale = models.ForeignKey(Sale, on_delete=models.CASCADE, related_name='sale_detail', default='',
                             blank=True, null=True)
    book = models.ForeignKey(Book, on_delete=models.CASCADE, related_name='book_reverse', default='')
    quantity = models.IntegerField(default=1, blank=True, null=True)
    price = models.DecimalField(blank=True, null=True, max_digits=6, decimal_places=2)

    def __str__(self):
        return "{0} - {1}".format("Sale", self.id)
