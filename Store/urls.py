from django.urls import path

from .views import *
from .helpers import *

urlpatterns = [
    path('author/', AuthorView.as_view()),
    path('author/<int:pk>/', AuthorUpdateView.as_view()),
    path('author/create', AuthorView.as_view()),

    path('book/', BookListView.as_view()),
    path('book/<int:pk>/', BookUpdateView.as_view()),
    path('book/create', BookCreateView.as_view()),

    path('book-author/', BookAuthorListView.as_view()),
    path('book-author/<int:pk>/', BookAuthorUpdateView.as_view()),
    path('book-author/create', BookAuthorCreateView.as_view()),

    path('customer/', CustomerListView.as_view()),
    path('customer/<int:pk>/', CustomerUpdateView.as_view()),
    path('customer/create', CustomerCreateView.as_view()),

    path('college/', CollegeListView.as_view()),
    path('college/<int:pk>/', CollegeUpdateView.as_view()),
    path('college/create', CollegeCreateView.as_view()),

    path('category/', CategoryListView.as_view()),
    path('category/<int:pk>/', CategoryUpdateView.as_view()),
    path('category/create', CategoryCreateView.as_view()),

    path('condition/', ConditionListView.as_view()),
    path('condition/<int:pk>/', ConditionUpdateView.as_view()),
    path('condition/create', ConditionCreateView.as_view()),

    path('type/', TypeListView.as_view()),
    path('type/<int:pk>/', TypeUpdateView.as_view()),
    path('type/create', TypeCreateView.as_view()),

    path('students/', StudentListView.as_view()),
    path('students/<int:pk>/', StudentUpdateView.as_view()),
    path('students/create', StudentCreateView.as_view()),

    path('sale-detail/', SaleDetailListView.as_view()),
    path('sale-detail/<int:pk>/', SaleDetailUpdateView.as_view()),
    path('sale-detail/create/', SaleDetailCreateView.as_view()),

    path('sale/', SaleListView.as_view()),
    path('sale/<int:pk>/', SaleUpdateView.as_view()),
    path('sale/create/', SaleCreateView.as_view()),


    path('sale/saledetail/create/', create_sale_and_sale_detail),
    path('create/sale-saledetail-credit/', sale_by_credit),
    path('create/sale-saledetail-credit-helper/', sale_by_credit_with_helper),


    path('report/book-by-author/', book_by_author),

    path('report/book-seller/', book_seller),

    path('report/order-by-price/', book_report_order_by_price),

    path('report/sold-filter-date/', book_filter_by_date),

    path('report/books-with-no-sales/', books_with_no_sale),

    path('report/customer-statistics/', customer_statistics),

    path('report/customer-books-category/', customer_books_category),

    path('report/books-by-type-college-condition/', list_books_by_type_college_condition),

    path('report/book-stock/', books_total_stock),

    path('report/check-current-book-price/', check_current_book_stock),

    # path('test/', ),



]
