from rest_framework.serializers import ModelSerializer, SerializerMethodField

from .models import Author, Book, BookAuthor, Customer, College, Category, Condition, Type, Student, Sale, SaleDetail


class AuthorSerializer(ModelSerializer):
    class Meta:
        model = Author
        fields = ['first_name', 'last_name', ]


class CategorySerializer(ModelSerializer):
    class Meta:
        model = Category
        fields = ['category_description', ]


class ConditionSerializer(ModelSerializer):
    class Meta:
        model = Condition
        fields = ['condition', ]


class TypeSerializer(ModelSerializer):
    class Meta:
        model = Type
        fields = ['type', ]


class CollegeSerializer(ModelSerializer):
    class Meta:
        model = College
        fields = ['college', ]


class StudentSerializer(ModelSerializer):
    full_name = SerializerMethodField()

    def get_full_name(self, student):
        return '{} {}'.format(student.first_name, student.last_name)

    class Meta:
        model = Student
        fields = ['first_name', 'last_name', 'full_name', 'index_number', 'college', 'email', 'phone']


class BookCreateSerializer(ModelSerializer):
    class Meta:
        model = Book
        fields = ['title', 'summary', 'category', 'condition', 'seller_type', 'student', 'price', 'stock']


class BookListUpdateSerializer(ModelSerializer):
    category = CategorySerializer()
    condition = ConditionSerializer()
    student = StudentSerializer()
    seller_type = TypeSerializer()

    class Meta:
        model = Book
        fields = ['title', 'summary', 'category', 'condition', 'seller_type', 'student', 'price', 'stock']

    def to_representation(self, instance):
        representation = super().to_representation(instance)
        student_representation = representation.pop('student')
        condition_representation = representation.pop('condition')
        category_representation = representation.pop('category')
        type_representation = representation.pop('seller_type')

        if type_representation is not None:
            for key in type_representation:
                representation[key] = type_representation[key]

        if category_representation is not None:
            for key in category_representation:
                representation[key] = category_representation[key]

        if condition_representation is not None:
            for key in condition_representation:
                representation[key] = condition_representation[key]

        if student_representation is not None:
            for key in student_representation:
                representation[key] = student_representation[key]

        return representation

    def to_internal_value(self, data):
        type_fields = [field.field_name for field in TypeSerializer()]
        category_fields = [field.field_name for field in CategorySerializer()]
        condition_fields = [field.field_name for field in ConditionSerializer()]
        student_fields = [field.field_name for field in StudentSerializer()]

        prepared_data, type_data, category_data, condition_data, student_data = {}, {}, {}, {}, {}

        for key, val in data.items():
            if key in type_fields:
                type_data[key] = val
            else:
                prepared_data[key] = val

        for key, val in data.items():
            if key in category_fields:
                category_data[key] = val
            else:
                prepared_data[key] = val

        for key, val in data.items():
            if key in condition_fields:
                condition_data[key] = val
            else:
                prepared_data[key] = val

        for key, val in data.items():
            if key in student_fields:
                student_data[key] = val
            else:
                prepared_data[key] = val

        prepared_data['seller_type'] = type_data
        prepared_data['category'] = category_data
        prepared_data['condition'] = condition_data
        prepared_data['student'] = student_data

        return super().to_internal_value(prepared_data)

    def update(self, instance, validated_data):
        instance.title = validated_data.get('title', instance.title)
        instance.summary = validated_data.get('summary', instance.summary)
        instance.price = validated_data.get('price', instance.price)
        instance.stock = validated_data.get('stock', instance.stock)

        if 'seller_type' in validated_data and validated_data['seller_type']:
            type_data = validated_data.pop('seller_type')
            instance.seller_type.type = type_data.get('type', instance.seller_type.type)

            instance.seller_type.save()

        if 'category' in validated_data and validated_data['category']:
            category_data = validated_data.pop('category')
            instance.category.category_description = category_data.get('category_description',
                                                                       instance.category.category_description)

            instance.category.save()

        if 'condition' in validated_data and validated_data['condition']:
            condition_data = validated_data.pop('condition')
            instance.condition.condition = condition_data.get('condition', instance.condition.condition)

            instance.condition.save()

        if 'student' in validated_data and validated_data['student']:
            student_data = validated_data.pop('student')
            instance.student.first_name = student_data.get('first_name', instance.student.first_name)
            instance.student.last_name = student_data.get('last_name', instance.student.last_name)
            instance.student.index_number = student_data.get('index_number', instance.student.index_number)
            instance.student.college = student_data.get('college', instance.student.college)
            instance.student.email = student_data.get('email', instance.student.email)
            instance.student.phone = student_data.get('phone', instance.student.phone)

            instance.student.save()

        return instance


class BookAuthorSerializer(ModelSerializer):
    class Meta:
        model = BookAuthor
        fields = ['author', 'book']


class CustomerSerializer(ModelSerializer):
    class Meta:
        model = Customer
        fields = ['first_name', 'last_name', 'email', 'phone', 'credit_card_id', 'credit']


class SaleDetailViewSerializer(ModelSerializer):
    """   Sale Detail View only shows the details of the sale    """

    class Meta:
        model = SaleDetail
        fields = ['sale', 'book', 'quantity', 'price']


class SaleDetailSerializer(ModelSerializer):
    class Meta:
        model = SaleDetail
        fields = ['id', 'sale', 'book', 'quantity', 'price']
        read_only_fields = ['price']


class SaleDetailTrySerializer(ModelSerializer):
    class Meta:
        model = SaleDetail
        fields = ['id', 'sale', 'book', 'quantity', 'price']
        read_only_fields = ['quantity']


class SaleSerializer(ModelSerializer):
    """
    Sale serializer that create's and updates sales
    """
    sale_detail = SaleDetailSerializer(many=True)

    class Meta:
        model = Sale
        fields = ['id', 'date', 'customer', 'sale_detail']

    def create(self, validated_data):
        sale_detail_data = validated_data.pop('sale_detail')
        sales = Sale.objects.create(**validated_data)
        for sale_detail in sale_detail_data:
            SaleDetail.objects.create(sale=sales, **sale_detail)

        return sales

    def update(self, instance, validated_data):
        instance.customer = validated_data.get('customer', instance.customer)
        instance.date = validated_data.get('date', instance.date)

        instance.save()

        sale_detail_data = validated_data.pop('sale_detail')
        sales_details = instance.sale_detail.all()
        sales_details = list(sales_details)

        for sale_detail in sale_detail_data:
            sale_details = sales_details.pop(0)
            sale_details.sale = sale_detail.get('sale', sale_details.sale)
            sale_details.book = sale_detail.get('book', sale_details.book)
            sale_details.quantity = sale_detail.get('quantity', sale_details.quantity)

            sale_details.save()

        return instance


