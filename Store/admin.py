from django.contrib import admin

# Register your models here.
from .models import Customer, Student, College, Type, Book, Condition, Author, Category, Sale, SaleDetail, BookAuthor, Parameter, CreditCard, CreditCardType


admin.site.register(Customer)
admin.site.register(College)
admin.site.register(Student)
admin.site.register(Type)
admin.site.register(Book)
admin.site.register(Condition)
admin.site.register(Author)
admin.site.register(Category)
admin.site.register(Sale)
admin.site.register(SaleDetail)
admin.site.register(BookAuthor)
admin.site.register(Parameter)
admin.site.register(CreditCard)
admin.site.register(CreditCardType)


