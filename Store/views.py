from django.db import connection
from rest_framework.decorators import api_view
from rest_framework.generics import RetrieveUpdateDestroyAPIView, CreateAPIView, ListAPIView
from rest_framework.response import Response
from rest_framework.views import APIView
from rest_framework import status, serializers
from rest_framework.permissions import AllowAny

from .helpers import *

from .models import Author, Book, BookAuthor, Customer, College, Category, Condition, Type, Student, SaleDetail, Sale, \
    Parameter
from .serializers import (AuthorSerializer, BookCreateSerializer, BookListUpdateSerializer, BookAuthorSerializer,
                          CustomerSerializer, CollegeSerializer,
                          CategorySerializer, ConditionSerializer, TypeSerializer, StudentSerializer,
                          SaleDetailSerializer, SaleSerializer, SaleDetailViewSerializer,
                          SaleDetailTrySerializer)


class AuthorView(APIView):
    """One view for create and retrieve to replace other views"""

    def get(self, request, format=None):
        queryset = Author.objects.all()
        serializer = AuthorSerializer(queryset, many=True)
        return Response(serializer.data)

    def post(self, request, format=None):
        serializer = AuthorSerializer(data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_201_CREATED)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


class AuthorListView(ListAPIView):
    """List all Authors in DB"""
    permission_classes = [AllowAny]
    serializer_class = AuthorSerializer
    queryset = Author.objects.all()


class AuthorUpdateView(RetrieveUpdateDestroyAPIView):
    """Opens Author whose <pk> we send and allows us to update that Author"""
    permission_classes = [AllowAny]
    serializer_class = AuthorSerializer
    queryset = Author.objects.all()


class AuthorCreateView(CreateAPIView):
    """Allows us to create an Author"""
    permission_classes = [AllowAny]
    serializer_class = AuthorSerializer
    queryset = Author.objects.all()


class BookListView(ListAPIView):
    permission_classes = [AllowAny]
    serializer_class = BookCreateSerializer
    queryset = Book.objects.all()


class BookUpdateView(RetrieveUpdateDestroyAPIView):
    permission_classes = [AllowAny]
    serializer_class = BookListUpdateSerializer
    queryset = Book.objects.all()


class BookCreateView(CreateAPIView):
    permission_classes = [AllowAny]
    serializer_class = BookListUpdateSerializer
    queryset = Book.objects.all()


class BookAuthorListView(ListAPIView):
    permission_classes = [AllowAny]
    serializer_class = BookAuthorSerializer
    queryset = BookAuthor.objects.all()


class BookAuthorUpdateView(RetrieveUpdateDestroyAPIView):
    permission_classes = [AllowAny]
    serializer_class = BookAuthorSerializer
    queryset = BookAuthor.objects.all()


class BookAuthorCreateView(CreateAPIView):
    permission_classes = [AllowAny]
    serializer_class = BookAuthorSerializer
    queryset = BookAuthor.objects.all()


class CustomerListView(ListAPIView):
    permission_classes = [AllowAny]
    serializer_class = CustomerSerializer
    queryset = Customer.objects.all()


class CustomerUpdateView(RetrieveUpdateDestroyAPIView):
    permission_classes = [AllowAny]
    serializer_class = CustomerSerializer
    queryset = Customer.objects.all()


class CustomerCreateView(CreateAPIView):
    permission_classes = [AllowAny]
    serializer_class = CustomerSerializer
    queryset = Customer.objects.all()


class CollegeListView(ListAPIView):
    permission_classes = [AllowAny]
    serializer_class = CollegeSerializer
    queryset = College.objects.all()


class CollegeUpdateView(RetrieveUpdateDestroyAPIView):
    permission_classes = [AllowAny]
    serializer_class = CollegeSerializer
    queryset = College.objects.all()


class CollegeCreateView(CreateAPIView):
    permission_classes = [AllowAny]
    serializer_class = CollegeSerializer
    queryset = College.objects.all()


class CategoryListView(ListAPIView):
    permission_classes = [AllowAny]
    serializer_class = CategorySerializer
    queryset = Category.objects.all()


class CategoryUpdateView(RetrieveUpdateDestroyAPIView):
    permission_classes = [AllowAny]
    serializer_class = CategorySerializer
    queryset = Category.objects.all()


class CategoryCreateView(CreateAPIView):
    permission_classes = [AllowAny]
    serializer_class = CategorySerializer
    queryset = Category.objects.all()


class ConditionListView(ListAPIView):
    permission_classes = [AllowAny]
    serializer_class = ConditionSerializer
    queryset = Condition.objects.all()


class ConditionUpdateView(RetrieveUpdateDestroyAPIView):
    permission_classes = [AllowAny]
    serializer_class = ConditionSerializer
    queryset = Condition.objects.all()


class ConditionCreateView(CreateAPIView):
    permission_classes = [AllowAny]
    serializer_class = ConditionSerializer
    queryset = Condition.objects.all()


class TypeListView(ListAPIView):
    permission_classes = [AllowAny]
    serializer_class = TypeSerializer
    queryset = Type.objects.all()


class TypeUpdateView(RetrieveUpdateDestroyAPIView):
    permission_classes = [AllowAny]
    serializer_class = TypeSerializer
    queryset = Type.objects.all()


class TypeCreateView(CreateAPIView):
    permission_classes = [AllowAny]
    serializer_class = TypeSerializer
    queryset = Type.objects.all()


class StudentListView(ListAPIView):
    permission_classes = [AllowAny]
    serializer_class = StudentSerializer
    queryset = Student.objects.all()


class StudentUpdateView(RetrieveUpdateDestroyAPIView):
    permission_classes = [AllowAny]
    serializer_class = StudentSerializer
    queryset = Student.objects.all()


class StudentCreateView(CreateAPIView):
    permission_classes = [AllowAny]
    serializer_class = StudentSerializer
    queryset = Student.objects.all()


class SaleDetailListView(ListAPIView):
    permission_classes = [AllowAny]
    serializer_class = SaleDetailViewSerializer
    queryset = SaleDetail.objects.all()


class SaleDetailUpdateView(RetrieveUpdateDestroyAPIView):
    permission_classes = [AllowAny]
    serializer_class = SaleDetailSerializer
    queryset = SaleDetail.objects.all()


class SaleDetailCreateView(CreateAPIView):
    """SaleDetail View that checks if the parameter in the Parameter table is True or False"""
    permission_classes = [AllowAny]

    qs = Parameter.objects.filter(id='1')
    parameter = qs[0]

    if parameter:
        serializer_class = SaleDetailSerializer

        def post(self, request, *args, **kwargs):
            return_data = []
            quantity = self.request.data.get('quantity')
            book_id = self.request.data.get('book')
            sale_id = self.request.data.get('sale')

            c = connection.cursor()
            qs = '''insert into store_saledetail(quantity, price, book_id, sale_id) values (''' + quantity + ''' , (''' + quantity + ''' * (select price from store_book where id = ''' + book_id + ''')), ''' + book_id + ''', ''' + sale_id + ''')'''

            qs_update = '''update store_book set store_book.stock = (store_book.stock - ''' + quantity + ''') where store_book.id = (''' + book_id + ''')'''

            qs2 = '''select max(id), price, quantity, book_id, sale_id from store_saledetail sd where sd.book_id = ''' + book_id + ''' and sd.quantity = ''' + quantity + ''' and sd.price = (''' + quantity + ''' * (select sb.price from store_book sb where id = ''' + book_id + ''')) limit 1'''

            c.execute(qs)
            c.execute(qs_update)
            c.execute(qs2)
            results = c.fetchall()

            for result in results:
                obj = {
                    'id': result[0],
                    'price': result[1],
                    'quantity': result[2],
                    'book': result[3],
                    'sale': result[4]
                }
                return_data.append(obj)

            c.close()

            return Response(return_data, status=201)

    elif not parameter:
        """If the parameter is false this part of If statement is run"""
        serializer_class = SaleDetailTrySerializer

        def post(self, request, *args, **kwargs):
            price = self.request.data.get('price')
            book_id = self.request.data.get('book')
            sale_id = self.request.data.get('sale')

            c = connection.cursor()

            qs_insert = '''insert into store_saledetail(quantity, price, book_id, sale_id) values (default , ''' + price + ''', ''' + book_id + ''',  ''' + sale_id + ''')'''

            """Updates stock in store_book table"""
            qs_update = '''update store_book set store_book.stock = (store_book.stock - 1) where store_book.id = (''' + book_id + ''')'''

            c.execute(qs_insert)
            c.execute(qs_update)
            c.close()

            return self.create(request, *args, **kwargs)


class SaleListView(ListAPIView):
    permission_classes = [AllowAny]
    serializer_class = SaleSerializer
    queryset = Sale.objects.all()


class SaleUpdateView(RetrieveUpdateDestroyAPIView):
    permission_classes = [AllowAny]
    serializer_class = SaleSerializer
    queryset = Sale.objects.all()


class SaleCreateView(CreateAPIView):
    permission_classes = [AllowAny]
    serializer_class = SaleSerializer
    queryset = Sale.objects.all()


@api_view(['GET', 'POST'])
def create_sale_and_sale_detail(request):
    if request.method == 'POST':
        c = connection.cursor()

        parameter = '''select parameter from store_parameter where id = '1' '''
        c.execute(parameter)
        parameter = c.fetchone()
        param = parameter[0]

        if param == 1:

            return_data = {}

            customer_id = request.data.get('customer_id')
            sale_detail = request.data['sale_detail']

            qs1 = '''START TRANSACTION;
                     SELECT 
                        @id := MAX(id) + 1
                     FROM
                        store_sale;
            
            insert into store_sale(id, date, customer_id) values(@id, now(), ''' + customer_id + ''');'''
            c.execute(qs1)

            for request in sale_detail:
                quantity = request['quantity']
                book_id = request['book']

                qs = '''insert into store_saledetail(quantity, price, book_id, sale_id) values (''' + quantity + ''' , (''' + quantity + ''' * (select price from store_book where id = ''' + book_id + ''')), ''' + book_id + ''', @id);'''

                c.execute(qs)

                qs_update = '''update store_book set store_book.stock = (store_book.stock - ''' + quantity + ''') where store_book.id = (''' + book_id + ''')'''
                c.execute(qs_update)

            c.execute(''' commit; ''')

            qs_select = ''' SELECT 
                                ss.id,
                                ss.customer_id,
                                ss.date,
                                sd.book_id,
                                sd.quantity,
                                format(sd.price, 2) as total
                            FROM
                                store_sale ss
                                    INNER JOIN
                                store_saledetail sd  on ss.id = sd.sale_id
                            WHERE
                                ss.id = @id;'''

            c.execute(qs_select)
            results = c.fetchall()

            temp = []

            for result in results:
                if results.index(result) == 0:
                    obj = {
                        'id': result[0],
                        'customer': result[1],
                        'date': result[2],
                        'sale_detail': []
                    }

                obj1 = {
                    "book": result[3],
                    "quantity": result[4],
                    "total": result[5]
                }
                temp.append(obj1)

                obj['sale_detail'] = temp

                return_data = obj

            c.close()

            return Response(return_data, status=201)

        else:

            return_data = {}

            customer_id = request.data.get('customer_id')
            sale_detail = request.data['sale_detail']

            qs1 = '''START TRANSACTION;
                                 SELECT 
                                    @id := MAX(id) + 1
                                 FROM
                                    store_sale;

                        insert into store_sale(id, date, customer_id) values(@id, now(), ''' + customer_id + ''');'''
            c.execute(qs1)

            for request in sale_detail:
                price = request['price']
                book_id = request['book']

                qs = '''insert into store_saledetail(quantity, price, book_id, sale_id) values (1, ''' + price + ''', ''' + book_id + ''', @id);'''

                c.execute(qs)

                qs_update = '''update store_book set store_book.stock = (store_book.stock - 1) where store_book.id = (''' + book_id + ''')'''
                c.execute(qs_update)

            c.execute(''' commit; ''')

            qs_select = ''' SELECT 
                                ss.id,
                                ss.customer_id,
                                ss.date,
                                sd.book_id,
                                sd.quantity,
                                format(sd.price, 2) as total
                            FROM
                                store_sale ss
                                    INNER JOIN
                                store_saledetail sd  on ss.id = sd.sale_id
                            WHERE
                                ss.id = @id ;'''

            c.execute(qs_select)
            results = c.fetchall()

            temp = []

            for result in results:
                if results.index(result) == 0:
                    obj = {
                        'id': result[0],
                        'customer': result[1],
                        'date': result[2],
                        'sale_detail': []
                    }

                obj1 = {
                    "book": result[3],
                    "quantity": result[4],
                    "total": result[5]
                }
                temp.append(obj1)

                obj['sale_detail'] = temp

                return_data = obj

            c.close()

            return Response(return_data, status=201)


@api_view()
def book_by_author(request):
    """
    Lists all books that were written by a chosen author.
    :param request:
    :return:
    """
    return_data = []
    author = request.data.get('author')

    c = connection.cursor()
    queryset = """select * from (SELECT
                           sb.title, sb.price, sb.stock, concat(sa.first_name, ' ', sa.last_name) as full_name
                       FROM
                           store_book sb
                               LEFT JOIN
                           store_bookauthor ba ON sb.id = ba.book_id
                               INNER JOIN
                           store_author sa ON ba.author_id = sa.id) temp where temp.full_name = '""" + author + """' """
    c.execute(queryset)
    results = c.fetchall()

    for result in results:
        obj = {
            'title': result[0],
            'price': result[1],
            'stock': result[2],
            'full_name': result[3]
        }
        return_data.append(obj)

    c.close()

    return Response(return_data, status=200)


@api_view()
def book_seller(request):
    """
    Lists book and their seller.
    :param request:
    :return:
    """
    return_data = []

    c = connection.cursor()
    queryset = """select sb.title as "title", concat(first_name,' ', st.last_name) as full_name, sty.type, sb.stock
                      from store_book sb left join store_student st on sb.student_id = st.id left join store_type sty on sb.seller_type_id = sty.id"""
    c.execute(queryset)
    results = c.fetchall()

    for result in results:
        obj = {
            'title': result[0],
            'full_name': result[1],
            'type': result[2],
            'stock': result[3]
        }
        return_data.append(obj)

    c.close()

    return Response(return_data, status=200)


@api_view()
def book_report_order_by_price(request):
    """
    Report that list books and orders them by pricing, and also limits to how many books are displayed with a default value
    :param request:
    :return:
    """
    return_data = []

    order_by = request.data.get('order_by')
    number = request.data.get('number')

    if number is None:
        number = '10'

    if order_by == 'low_to_high':
        order_by = 'ASC'
    elif order_by == 'high_to_low':
        order_by = 'DESC'
    else:
        order_by = 'ASC'

    c = connection.cursor()

    queryset = """  select 
                        sb.title, concat(st.first_name, ' ', st.last_name) as full_name,  ty.type, sb.stock, sb.price
                    from 
                        store_book sb
                            left join
                        store_student st on sb.student_id = st.id
                            left join
                                store_type ty on sb.seller_type_id = ty.id
                    order by sb.price """ + order_by + """ limit """ + number + """ """

    c.execute(queryset)

    results = c.fetchall()

    for result in results:
        obj = {
            'title': result[0],
            'full_name': result[1],
            'type': result[2],
            'stock': result[3],
            'price': result[4]
        }
        return_data.append(obj)

    c.close()

    return Response(return_data)


@api_view()
def book_filter_by_date(request):
    """
    Filter sales by date from <date> to <date>, sum their sales by book and count how many books were sold.
    :param request:
    :return:
    """
    return_data = []

    from_date = request.data.get('from_date')
    to_date = request.data.get('to_date')
    order_by = request.data.get('order_by')

    if order_by == 'low_to_high':
        order_by = 'ASC'
    elif order_by == 'high_to_low':
        order_by = 'DESC'
    else:
        order_by = 'ASC'

    c = connection.cursor()

    queryset = """  SELECT 
                        sb.title,
                        SUM(sd.quantity) AS quantity,
                        SUM(sd.price) AS total
                    FROM
                        store_sale ss
                            INNER JOIN
                        store_saledetail sd ON ss.id = sd.sale_id
                            INNER JOIN
                        store_book sb ON sd.book_id = sb.id
                    WHERE
                        ss.date BETWEEN '""" + from_date + """'
                            AND '""" + to_date + """'
                    GROUP BY sb.title
                    ORDER BY total """ + order_by + """ """

    c.execute(queryset)
    results = c.fetchall()

    for result in results:
        obj = {
            'title': result[0],
            'quantity': result[1],
            'total': result[2]
        }
        return_data.append(obj)

    c.close()

    return Response(return_data)


@api_view()
def books_with_no_sale(request):
    """
    Report that returns all books with no sales and filters the books by date.
    :param request:
    :return:
    """
    return_data = []

    from_date = request.data.get('from_date')
    to_date = request.data.get('to_date')

    c = connection.cursor()

    queryset = """ SELECT 
                        sb.title
                    FROM
                        store_book sb
                    WHERE
                        id NOT IN (SELECT 
                                sb.id
                            FROM
                                store_sale ss
                                    INNER JOIN
                                store_saledetail sd ON ss.id = sd.sale_id
                                    INNER JOIN
                                store_book sb ON sd.book_id = sb.id
                            WHERE
                                ss.date BETWEEN '""" + from_date + """'
                            AND '""" + to_date + """'
                            GROUP BY sb.id)"""

    c.execute(queryset)
    results = c.fetchall()

    for result in results:
        obj = {
            'title': result[0],
        }
        return_data.append(obj)

    c.close()

    return Response(return_data)


@api_view()
def customer_statistics(request):
    return_data = []
    customer_id = request.data.get('customer_id')
    from_date = request.data.get('from_date')
    to_date = request.data.get('to_date')
    full_name = request.data.get('full_name')

    c = connection.cursor()

    if customer_id is None:
        customer_id = ''
    elif full_name is None:
        customer_id = 'temp.id = ' + customer_id + ''
    else:
        customer_id = 'temp.id = ' + customer_id + ' AND'

    if full_name is None:
        name = ''
    else:
        name = '(temp.full_name = "' + full_name + '" OR temp.full_name LIKE "%' + full_name + '%")'

    queryset = """
                SELECT 
                    temp.full_name,
                    temp.quantity,
                    temp.total,
                    temp.id as customer_id
                FROM
                    (SELECT 
                        CONCAT(sc.first_name, ' ', sc.last_name) AS full_name,
                        FORMAT(SUM(sd.quantity), 0) AS quantity,
                        FORMAT(SUM(sd.price), 2) AS total,
                        sc.id
                    FROM
                        store_sale ss
                            INNER JOIN 
                        store_saledetail sd ON sd.sale_id = ss.id
                            INNER JOIN 
                        store_customer sc ON ss.customer_id = sc.id
                            INNER JOIN 
                        store_book sb ON sd.book_id = sb.id
                    WHERE
                        ss.date BETWEEN '""" + from_date + """' AND '""" + to_date + """'
                    GROUP BY full_name) AS temp
                WHERE
                     """ + customer_id + """ """ + name + """
                ORDER BY temp.full_name
               """
    c.execute(queryset)
    results = c.fetchall()

    for result in results:
        obj = {
            'customer_id': result[3],
            'full_name': result[0],
            'quantity': result[1],
            'total': result[2]
        }
        return_data.append(obj)

    c.close()

    return Response(return_data)


@api_view()
def customer_books_category(request):
    return_data = []

    from_date = request.data.get('from_date')
    to_date = request.data.get('to_date')
    full_name = request.data.get('full_name')

    if full_name is None:
        name = ''
    else:
        name = 'HAVING full_name = "' + full_name + '" OR full_name LIKE "%' + full_name + '%" '

    if from_date is None and to_date is None:
        where = ''
    else:
        where = ' WHERE ss.date BETWEEN "' + from_date + '" AND "' + to_date + '" '

    c = connection.cursor()

    queryset = """  SELECT 
                        CONCAT(sc.first_name, ' ', sc.last_name) AS full_name,
                        ct.category_description AS category,
                        SUM(sd.quantity) AS quantity
                    FROM
                        store_customer sc
                            INNER JOIN
                        store_sale ss ON sc.id = ss.customer_id
                            INNER JOIN
                        store_saledetail sd ON ss.id = sd.sale_id
                            INNER JOIN
                        store_book sb ON sd.book_id = sb.id
                            INNER JOIN
                        store_category ct ON sb.category_id = ct.id
                    """ + where + """
                    GROUP BY category , full_name
                    """ + name + """
     """
    c.execute(queryset)

    results = c.fetchall()

    for result in results:
        obj = {
            'full_name': result[0],
            'category': result[1],
            'quantity': result[2]
        }
        return_data.append(obj)

    c.close()

    return Response(return_data)


@api_view()
def list_books_by_type_college_condition(request):
    return_data = []

    seller_type = request.data.get('seller_type')
    college = request.data.get('college')
    condition = request.data.get('condition')

    where = ''
    if seller_type is None and college is None and condition is None:
        where = where
    elif seller_type is None and college is None:
        where = 'WHERE di.condition = "' + condition + '" OR di.condition LIKE "%' + condition + '%"'
    elif seller_type is None and condition is None:
        where = 'WHERE co.college = "' + college + '" OR co.college LIKE "%' + college + '%"'
    elif condition is None and college is None:
        where = 'WHERE ty.type = "' + seller_type + '" OR ty.type LIKE "%' + seller_type + '%"'
    elif seller_type is None:
        where = 'WHERE (di.condition = "' + condition + '" OR di.condition LIKE "%' + condition + '%") AND (co.college = "' + college + '" OR co.college LIKE "%' + college + '%")'
    elif condition is None:
        where = 'WHERE (ty.type = "' + seller_type + '" OR ty.type LIKE "%' + seller_type + '%") AND (co.college = "' + college + '" OR co.college LIKE "%' + college + '%")'
    elif college is None:
        where = 'WHERE (ty.type = "' + seller_type + '" OR ty.type LIKE "%' + seller_type + '%") AND (di.condition = "' + condition + '" OR di.condition LIKE "%' + condition + '%")'
    else:
        where = 'WHERE (ty.type = "' + seller_type + '" OR ty.type LIKE "%' + seller_type + '%") AND (co.college = "' + college + '" OR co.college LIKE "%' + college + '%") AND (di.condition = "' + condition + '" OR di.condition LIKE "%' + condition + '%")'

    c = connection.cursor()

    queryset = """
                SELECT 
                    sb.title, 
                    sb.price,
                    (CONCAT(st.first_name, ' ', st.last_name)) AS full_name,
                    ty.type, 
                    co.college, 
                    di.condition
                FROM
                    store_book sb
                        LEFT JOIN
                    store_student st ON sb.student_id = st.id
                        LEFT JOIN
                    store_college co ON st.college_id = co.id
                        LEFT JOIN
                    store_type ty ON sb.seller_type_id = ty.id
                        LEFT JOIN
                    store_condition di ON sb.condition_id = di.id
                """ + where + """
               """
    c.execute(queryset)

    results = c.fetchall()

    for result in results:
        obj = {
            'title': result[0],
            'price': result[1],
            'full_name': result[2],
            'type': result[3],
            'college': result[4],
            'condition': result[5]
        }
        return_data.append(obj)

    c.close()

    return Response(return_data)


@api_view()
def books_total_stock(request):
    return_data = []

    order_by = request.data.get('order_by')

    if order_by == 'low_to_high':
        order_by = 'ASC'
    elif order_by == 'high_to_low':
        order_by = 'DESC'
    else:
        order_by = 'ASC'

    c = connection.cursor()

    queryset = """
                SELECT 
                    title,
                    price,
                    stock,
                    FORMAT(SUM(price * stock), 2) AS total
                FROM
                    store_book
                GROUP BY 
                    title
                ORDER BY 
                    SUM(price * stock) """ + order_by + """
               """
    c.execute(queryset)

    results = c.fetchall()

    for result in results:
        obj = {
            'title': result[0],
            'price': result[1],
            'stock': result[2],
            'total': result[3]
        }
        return_data.append(obj)

    c.close()

    return Response(return_data)


@api_view(['GET', 'POST'])
def sale_by_credit(request):
    if request.method == 'POST':

        c = connection.cursor()

        return_data = {}

        customer_id = request.data.get('customer_id')
        sale_detail = request.data['sale_detail']

        qs1 = '''START TRANSACTION;
                             SELECT 
                                @id := MAX(id) + 1
                             FROM
                                store_sale;
    
                    insert into store_sale(id, date, customer_id) values(@id, now(), ''' + customer_id + ''');'''
        c.execute(qs1)

        for request in sale_detail:
            quantity = request['quantity']
            book_id = request['book']

            qs = '''insert into store_saledetail(quantity, price, book_id, sale_id) values (''' + quantity + ''' , (''' + quantity + ''' * (select price from store_book where id = ''' + book_id + ''')), ''' + book_id + ''', @id);'''

            c.execute(qs)

            qs_update = '''update store_book set store_book.stock = (store_book.stock - ''' + quantity + ''') where store_book.id = (''' + book_id + ''')'''
            c.execute(qs_update)

            qs_select = ''' SELECT 
                                format(sd.price, 2) as total
                            FROM
                                store_sale ss
                                    INNER JOIN
                                store_saledetail sd  on ss.id = sd.sale_id
                            WHERE
                                ss.id = @id;'''
            c.execute(qs_select)
            results = c.fetchall()

            for result in results:
                total = result[0]

                total_num = float(total)

                qs_credits = ("""SELECT 
                                    sc.credit
                                FROM
                                    store_customer sc
                                WHERE
                                    id = '""" + customer_id + """' """)
                c.execute(qs_credits)
                credit_results = c.fetchall()

                for credit_result in credit_results:
                    credit_num = credit_result[0]

                    if total_num > credit_num:
                        c.execute(""" rollback; """)
                        raise serializers.ValidationError({'credit': 'insufficient funds'})

            credit_var = str(credit_num)

            qsc_update = """UPDATE
                                store_customer
                            SET
                                credit = """ + credit_var + """ - """ + total + """
                            WHERE
                                id = """ + customer_id + """; """
            c.execute(qsc_update)

        c.execute(''' commit; ''')

        qs_select = ''' SELECT 
                                        ss.id,
                                        ss.customer_id,
                                        ss.date,
                                        sd.book_id,
                                        sd.quantity,
                                        format(sd.price, 2) as total
                                    FROM
                                        store_sale ss
                                            INNER JOIN
                                        store_saledetail sd  on ss.id = sd.sale_id
                                    WHERE
                                        ss.id = @id;'''

        c.execute(qs_select)
        results = c.fetchall()

        temp = []

        for result in results:
            if results.index(result) == 0:
                obj = {
                    'id': result[0],
                    'customer': result[1],
                    'date': result[2],
                    'sale_detail': []
                }

            obj1 = {
                "book": result[3],
                "quantity": result[4],
                "total": result[5]
            }
            temp.append(obj1)

            obj['sale_detail'] = temp

            return_data = obj

        c.close()

        return Response(return_data, status=201)


@api_view(['GET', 'POST'])
def sale_by_credit_with_helper(request):
    """
    First this method checks the total price of all books and compares to the customers current credit.
    If the customer has enough credit we start the transaction. If customer doesn't have enough credit ValidationError.
    There are several helper functions's used in this function.
    :param request:
    :return:
    """
    if request.method == 'POST':

        c = connection.cursor()

        return_data = {}

        customer_id = request.data.get('customer_id')

        if customer_id is None:
            raise serializers.ValidationError({'Please enter': 'customer_id'})
            # customer_id = '1'

        sale_detail = request.data['sale_detail']

        total_price = check_total_price(sale_detail)

        qs_credits = check_customer_credit(customer_id)

        if total_price > qs_credits:
            raise serializers.ValidationError({'credit': 'insufficient funds'})

        qs1 = '''START TRANSACTION;
                             SELECT 
                                @id := MAX(id) + 1
                             FROM
                                store_sale;

                    insert into store_sale(id, date, customer_id) values(@id, now(), ''' + customer_id + ''');'''
        c.execute(qs1)

        for request in sale_detail:
            quantity = request['quantity']
            book_id = request['book']

            qs = '''INSERT INTO store_saledetail(quantity, price, book_id, sale_id) VALUES (''' + quantity + ''' , (''' + quantity + ''' * (SELECT price FROM store_book WHERE id = ''' + book_id + ''')), ''' + book_id + ''', @id);'''

            c.execute(qs)

            update_book_stock(quantity, book_id)

        update_customer_credit(total_price, customer_id)

        c.execute(''' commit; ''')

        qs_select = ''' SELECT 
                            ss.id,
                            ss.customer_id,
                            ss.date,
                            sd.book_id,
                            sd.quantity,
                            format(sd.price, 2) as total
                        FROM
                            store_sale ss
                                INNER JOIN
                            store_saledetail sd  on ss.id = sd.sale_id
                        WHERE
                            ss.id = @id;'''

        c.execute(qs_select)
        results = c.fetchall()

        temp = []

        for result in results:
            if results.index(result) == 0:
                obj = {
                    'id': result[0],
                    'customer': result[1],
                    'date': result[2],
                    'sale_detail': [],
                    'total_price': total_price
                }

            obj1 = {
                "book": result[3],
                "quantity": result[4],
                "total": result[5]
            }
            temp.append(obj1)

            obj['sale_detail'] = temp

            return_data = obj

        c.close()

        return Response(return_data, status=201)


@api_view()
def check_current_book_stock(request):
    return_data = {}

    c = connection.cursor()

    queryset = """  SELECT 
                        title, stock, price, FORMAT(SUM(price * stock), 2) AS total
                    FROM
                        store_book
                    GROUP BY title"""

    c.execute(queryset)

    results = c.fetchall()

    total = check_books_stock_sum()

    temp = []

    for result in results:
        if results.index(result) == 0:
            obj = {
                'books': [],
                'total': total
            }
        obj1 = {
            'title': result[0],
            'stock': result[1],
            'price': result[2],
            'total': result[3]
        }
        temp.append(obj1)

        obj['books'] = temp

        return_data = obj

    c.close()

    return Response(return_data)
