# Generated by Django 2.1.4 on 2019-03-15 14:44

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('Store', '0010_auto_20190314_1053'),
    ]

    operations = [
        migrations.AlterField(
            model_name='book',
            name='price',
            field=models.DecimalField(decimal_places=2, max_digits=6),
        ),
        migrations.AlterField(
            model_name='customer',
            name='credit',
            field=models.DecimalField(blank=True, decimal_places=2, default=0, max_digits=6, null=True),
        ),
        migrations.AlterField(
            model_name='saledetail',
            name='price',
            field=models.DecimalField(blank=True, decimal_places=2, max_digits=6, null=True),
        ),
    ]
